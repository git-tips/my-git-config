## --global format.pretty
``` js
git config --global format.pretty "%C(red)%h %Cgreen(%cD)%Creset %s %C(bold blue)<%an>%C(yellow)%d%Creset"
```
## Git lg
Alias log for 10 records | [Thank](https://coderwall.com/p/euwpig/a-better-git-log)
``` js
git config --global alias.lg "log --color --graph --abbrev-commit -10"
```
![console style](https://gitlab.com/git-tips/my-git-config/-/raw/d6ba5b17752087e3f593e3e6f1ac582ce7ded61e/image/example_git_lg.png)
